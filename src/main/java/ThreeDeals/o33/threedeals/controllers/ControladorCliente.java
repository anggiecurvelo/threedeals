package threedeals.o33.threedeals.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import threedeals.o33.threedeals.entities.Cliente;
import threedeals.o33.threedeals.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ControladorCliente {
    @Autowired
    ClienteService clienteService;

    @GetMapping()
    public ArrayList<Cliente> obtenerCliente(){
        return clienteService.obtenerCliente();
    }
    
    @PostMapping()
    public Cliente guardarCliente(@RequestBody Cliente cliente){
        return this.clienteService.guardarcliente(cliente);
    }
}
