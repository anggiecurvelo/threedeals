package threedeals.o33.threedeals.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import threedeals.o33.threedeals.dao.PedidoRepo;
import threedeals.o33.threedeals.entities.Pedido;

@Service
public class PedidoService {
    @Autowired
    PedidoRepo pedidoRepo;

    public ArrayList<Pedido> crearPedido(){
        return (ArrayList<Pedido>) pedidoRepo.findAll();
    }

    public Pedido guardarpedido(Pedido pedido){
        return pedidoRepo.save(pedido);
    }
}
