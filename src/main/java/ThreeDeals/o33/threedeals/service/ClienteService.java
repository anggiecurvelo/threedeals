package threedeals.o33.threedeals.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import threedeals.o33.threedeals.dao.ClienteRepo;
import threedeals.o33.threedeals.entities.Cliente;

@Service
public class ClienteService {
    @Autowired
    ClienteRepo clienteRepo;

    public ArrayList<Cliente> obtenerCliente(){
        return (ArrayList<Cliente>) clienteRepo.findAll();
    }

    public Cliente guardarcliente(Cliente cliente){
        return clienteRepo.save(cliente);
    }


}
