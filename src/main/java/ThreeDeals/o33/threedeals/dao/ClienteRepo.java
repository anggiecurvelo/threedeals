package threedeals.o33.threedeals.dao;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import threedeals.o33.threedeals.entities.Cliente;

@Repository
public interface ClienteRepo extends CrudRepository<Cliente, Integer>{
    
}
