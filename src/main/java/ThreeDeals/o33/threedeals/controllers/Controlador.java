package threedeals.o33.threedeals.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import threedeals.o33.threedeals.dao.ClienteRepo;
import threedeals.o33.threedeals.entities.Cliente;



@RestController
public class Controlador {
    
    @Autowired
    ClienteRepo clienteRepo;

    @PostMapping("/agregar-cliente")
    public Cliente idCliente(@RequestBody Cliente newCliente){
        return clienteRepo.save(newCliente);
    }

    @GetMapping("/get-cliente")
    public Iterable<Cliente> getCliente(){
        return clienteRepo.findAll();
    }

    
    
}
