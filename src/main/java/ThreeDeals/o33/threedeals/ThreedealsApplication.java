package threedeals.o33.threedeals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThreedealsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThreedealsApplication.class, args);
	}

}
