package threedeals.o33.threedeals.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import threedeals.o33.threedeals.entities.Pedido;
import threedeals.o33.threedeals.service.PedidoService;

@RestController
@RequestMapping("/pedido")
public class ControladorPedido {
    @Autowired
    PedidoService pedidoService;

    @GetMapping()
    public ArrayList<Pedido> crearPedido(){
        return pedidoService.crearPedido();
    }
    
    @PostMapping()
    public Pedido guardarPedido(@RequestBody Pedido pedido){
        return this.pedidoService.guardarpedido(pedido);
    }
}
