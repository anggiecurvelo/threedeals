package ThreeDeals.ThreeDeals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThreeDealsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThreeDealsApplication.class, args);
	}

}
